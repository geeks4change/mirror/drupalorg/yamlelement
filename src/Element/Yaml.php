<?php

namespace Drupal\yamlelement\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Textarea;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml as SymfonyYaml;

/**
 * Provides a form element for yaml input via textarea.
 *
 * @FormElement("yaml")
 */
class Yaml extends Textarea {

  /**
   * @inheritDoc
   */
  public function getInfo() {
    $info = parent::getInfo();
    $info['#element_validate'][] = [static::class, 'validateYaml'];
    $info['#pre_render'][] = [static::class, 'preRenderYaml'];
    return $info;
  }

  /**
   * Element #pre_render callback
   */
  public static function preRenderYaml($element) {
    $element['#value'] = SymfonyYaml::dump($element['#value'], 999, 2, SymfonyYaml::DUMP_EXCEPTION_ON_INVALID_TYPE | SymfonyYaml::DUMP_MULTI_LINE_LITERAL_BLOCK);
    return $element;
  }

  /**
   * Element element_validate callback
   */
  public static function validateYaml($element, FormStateInterface $form_state, $form) {
    $input = $form_state->getValue($element['#parents']);
    try {
      $value = SymfonyYaml::parse($input, SymfonyYaml::PARSE_EXCEPTION_ON_INVALID_TYPE);
    } catch (ParseException $e) {
      $form_state->setError($element, t('The Yaml in %field is not valid.', ['%field' => $element['#title']]));
    }
    if (isset($value)) {
      $form_state->setValue($element['#parents'], $value);
    }
  }

}
